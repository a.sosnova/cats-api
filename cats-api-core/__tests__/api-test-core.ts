import Client from '../../dev/http-client';
import type { Cat, CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Римусякаколбасяка', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('Core API', () => {
  beforeEach(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterEach(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

    describe('API поиска котиков по id', () => {

      it('Поиск котика по id', async () => {
        const response: any = await HttpClient.get(`core/cats/get-by-id/?id=${catId}`, {
          responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);
        
        expect(response.body.cat).toEqual({
          id: catId,
          ...cats[0],
          tags: null,
          likes: 0,
          dislikes: 0,
        });
      });

      it('Попытка получить котика с неверным форматом id', async () => {
        await expect(
          HttpClient.get(`core/cats/get-by-id/?id=${fakeId}`, {
            responseType: 'json',
          })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
      });
    })

    describe('API добавления описания котику', () => {

        it('Добавление описания котику', async() => {
          const newDescription = 'Очень крутое описание котика';
          const response: any = await HttpClient.post('core/cats/save-description', {
            responseType: 'json',
            json: {
              catId: catId,
              catDescription: newDescription
            }
          })
          expect(response.statusCode).toEqual(200)
          expect(response.body).toEqual({
            id: catId,
            ...cats[0],
            description: newDescription,
            tags: null,
            likes: 0,
            dislikes: 0,
          });
        })
    })

    describe('API списка котов, сгруппированного по группам', () => {

      it('Получение списка котов, сгруппированного по группам', async() => {
        const response: any = await HttpClient.get('core/cats/allByLetter', {
          responseType: 'json',
        })
        expect(response.statusCode).toEqual(200)
        expect(response.body).toMatchObject({
          groups: expect.arrayContaining([
            expect.objectContaining({
              title: expect.any(String),
              count_in_group: expect.any(Number),
              count_by_letter: expect.any(Number),
              cats: expect.arrayContaining([
                expect.objectContaining({
                  id: expect.any(Number),
                  name: expect.any(String),
                  description: expect.any(String),
                  gender: expect.any(String),
                  likes: expect.any(Number),
                  dislikes: expect.any(Number),
                  count_by_letter: expect.any(String),
                })
              ])
            })
          ]),
          count_output: expect.any(Number),
          count_all: expect.any(Number),
        })
      })

      it("Проверка корректности счетчиков в группах", async() => {
        const response: any = await HttpClient.get('core/cats/allByLetter', {
          responseType: 'json',
        })
          const groups = response.body.groups
          let count_by_letter = 0
          let count_output = 0
          for (let group of groups) {
              expect(group.cats.length).toEqual(group.count_in_group)
              count_by_letter += group.count_by_letter
              count_output += group.count_in_group
          }
          expect(response.body.count_output).toEqual(count_output)
          expect(response.body.count_all).toEqual(count_by_letter)    
      })
   
  })

});
